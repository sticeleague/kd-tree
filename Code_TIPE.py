import numpy as np
import matplotlib.pyplot as plt
import random as rd
from math import *
import time

#Conctruction de l'arbre
                     
def separation (P,direction,Arbre,i):
    if P==[]:
        return 'Vide'
    else:
       n=len(P)
       N=n//2
       direction2=((direction % 3)+1) #Changement de direction
       P.sort(key=lambda b:b[direction-1]) #nlog(n)
       mediane=P[N] #On cherche l'élément de position n//2 dans la direction souhaitée
       Arbre[i]=mediane
       return((P[:N],direction2,2*i+1),(P[N+1:],direction2, 2*i+2))

def construction(P):
    Arbre={}
    L=[(P,1,0)]
    while L!=[]:
        lst,direction,i=L.pop()
        C=separation(lst,direction,Arbre,i)
        if C!='Vide':
            a,b,c=C[0]
            a2,b2,c2=C[1]
            L.append((a,b,c))
            L.append((a2,b2,c2))
    return Arbre

def test_temps():
    X=[int(1e5),250000,500000,750000,int(1e6),2500000,5000000,7500000,int(1e7)]
    Y_tree=[]
    Y_simple=[]
    for i in X:
        P=points_aleatoires(i)
        A=construction(P)
        start=time.perf_counter()
        Res=kpp_simple(100,(0,0,0),P)
        end=time.perf_counter()
        Y_simple.append(end-start)
        start=time.perf_counter()
        Res=kpp(100,(0,0,0),A)
        end=time.perf_counter()
        Y_tree.append(end-start)
    #plt.plot(X,Y_tree,'o',color='b')
    #plt.plot(X,Y_simple,'o', color='r')
    return Y_tree,Y_simple

def test():
    X=[int(1e5),250000,500000,750000,int(1e6),2500000,5000000,7500000,int(1e7)]
    n=len(X)
    Ds={}
    Dt={}
    for i in range(n):
        Ds[i]=0
        Dt[i]=0
    for i in range(5):
        Yt,Ys=test_temps()
        for j in range(n):
            Ds[j]+=Ys[j]/5
            Dt[j]+=Yt[j]/5
        print(i)
    Y_tree,Y_simple=[],[]
    for i in range(n):
        Y_tree.append(Dt[i])
        Y_simple.append(Ds[i])
    plt.plot(X,Y_tree,'o',color='b')
    plt.plot(X,Y_simple,'o', color='r')
    return X,Y_tree,Y_simple

#Plus proches voisins

def dist(P,Q):
    a,b,c=P
    d,e,f=Q
    return sqrt((a-d)**2+(b-e)**2+(c-f)**2)

"""def kpp_classique(n,P,L):
    assert(len(L)>n)
    distance=[]
    for e in L:
        d=dist(e,P)
        distance.append((e,d))
    distance.sort(key=lambda b:b[1])
    res=[]
    for i in range(n):
        res.append(distance[i][0])
    return res"""

def kpp_simple(n,P,L):
    assert(len(L)>n)
    distance=[]
    for e in L:
        d=dist(e,P)
        distance.append((e,d))
    res=distance[:n]
    res.sort(key=lambda b:b[1])
    dmax=res[-1][1]
    for e in distance:
        pt,d=e
        if d<dmax:
            res=triinsertion(n,e,res)
            dmax=res[-1][1]
    return [e[0] for e in res]

def triinsertion(n,p,L):
    i=0
    res=[]
    while L[i][1]<p[1]:
        res.append(L[i])
        i+=1
    res.append(p)
    while i<n-1:
        res.append(L[i])
        i+=1
    return res     

def recherche(P,A,dist):
    L=[]
    vis=[(0,0)]
    while len(vis)>0:
        i,direction=vis.pop()
        if i in A:
            pt=A[i]
            if abs(pt[direction]-P[direction])<=dist: # On cherche les points dans un cube de coté dist
                if dans(pt,dist,P):
                    L.append(pt)
                vis.append((2*i+1,(direction+1)%3))
                vis.append((2*i+2,(direction+1)%3))
            elif pt[direction]<(P[direction]-dist):
                vis.append((2*i+2,(direction+1)%3))
            else:
                vis.append((2*i+1,(direction+1)%3))
    return L

def dans(pt,dist,P):
    a,b,c=pt
    A,B,C=P
    if abs(a-A)<=dist and abs(b-B)<=dist and abs(c-C)<=dist:
        return True
    return False

def check(L,P,d):
    i=0
    for e in L:
        if dist(e,P)<=d:
            i+=1
    return i

def kpp(n,P,A,dist=10):
    d=dist
    Voisins=recherche(P,A,d)
    taille=check(Voisins,P,d)
    while taille!=n:
        if taille>n:
            Voisins=kpp_simple(n,P,Voisins)
        else:
            d+=d
            Voisins=recherche(P,A,d) #On augmente la taille du cube de recherche
        taille=check(Voisins,P,d)
    return Voisins

#Dimension d'un objet 

def covariance(A,B):
    n=len(A)
    Cov,SA,SB=0,0,0
    for i in range(n):
        Cov+=(1/n)*(A[i]*B[i])
        SA+=(1/n)*A[i]
        SB+=(1/n)*B[i]
    return Cov-SA*SB

def dim (L):
    X,Y,Z=[],[],[]
    for P in L:
        x,y,z= P
        X.append(x),Y.append(y),Z.append(z)
    C=np.zeros((3,3))
    C[0][0]=covariance(X,X)
    C[0][1]=covariance(X,Y)
    C[0][2]=covariance(X,Z)
    C[1][1]=covariance(Y,Y)
    C[1][2]=covariance(Y,Z)
    C[2][2]=covariance(Z,Z)
    for i in range (3):
        for j in range(i+1):
            C[i][j]=C[j][i]
    tab=np.linalg.eig(C)[0]
    print(C)
    print(tab)
    return

#Fonctions de test 

def points_aleatoires(n):
    P=[]
    for i in range (n):
        pt=(rd.randint(-1000,1001),rd.randint(-1000,1001),rd.randint(-1000,1001))
        P.append(pt)
    return P

def represente(L):
    X=[]
    Y=[]
    Z=[]
    fig = plt.figure()
    ax=plt.axes(projection='3d')
    for e in L:
        x,y,z=e
        X.append(x)
        Y.append(y)
        Z.append(z)
    ax.scatter(X,Y,Z,'green')
    return

def rep_kpp(vois,L,pt):
    cop=L.copy()
    cop2=vois.copy()
    cop2.remove(pt)
    cop.remove(pt)
    X,Y,Z=[],[],[]
    X2,Y2,Z2=[],[],[]
    fig = plt.figure()
    ax=plt.axes(projection='3d')
    for e in cop2:
        cop.remove(e)
        x,y,z=e
        X2.append(x)
        Y2.append(y)
        Z2.append(z)
    for e in cop:
        x,y,z=e
        X.append(x)
        Y.append(y)
        Z.append(z)
    x,y,z=pt
    X3,Y3,Z3=[x],[y],[z]
    ax.scatter(X3,Y3,Z3,color='black')
    ax.scatter(X,Y,Z)
    ax.scatter(X2,Y2,Z2,color='red')
    return

def creerbarre(D,F,k): #arg: pt de depart et d'arrivée, nb de points
    L=[]
    if D==F:
        return "Même point"
    xd,yd,zd=D
    xf,yf,zf=F
    dx,dy,dz=abs(xd-xf)/(k-1),abs(yd-yf)/(k-1),abs(zd-zf)/(k-1)
    for i in range (k):
        L.append((xd+i*dx,yd+i*dy,zd+i*dz))
    represente(L)
    return L

def creermur(l,h,k,P,direction): #Arg: longueur, hauteur, nb de points, Point central, selon x ou y
    res=[]
    p1,p2,p3=P
    if direction=='x':
        L=p1-l/2
        for i in range(k):
            res.append((rd.uniform(L,L+l),p2,rd.uniform(p3,p3+h)))
    else:
        L=p2-l/2
        for i in range (k):
            res.append((p1,rd.uniform(L,L+l),rd.uniform(p3,p3+h)))
    represente(res)
    return res

def creersphere(P,R,k): #Arg: centre, rayon, nb de points
    x,y,z=P
    L=[]
    for i in range(k):
        Q=(rd.uniform(-1,1), rd.uniform(-1,1) , rd.uniform(-1,1))
        a,b,c=renormaliser(Q,R)
        L.append((a+x,b+y,c+z))
    represente(L)
    return L

def testeur(L1,L2):
    for e in L1:
        if e not in L2:
            print(e,'pas dans L2')
    for e in L2:
        if e not in L1:
            print(e,'pas dans L1')
    return
        

#Fonctions annexes

def inf ( pt1, pt2, direction):
    if direction==1:
        if pt1[0]<pt2[0]:
            return True
    elif direction==2:
        if pt1[1]<pt2[1]:
            return True
    else:
        if pt1[2]<pt2[2]:
            return True
    return False

def norme(Q):
    x,y,z=Q
    return sqrt(x**2+ y**2 +z**2)

def renormaliser(Q,R):
    n=norme(Q)
    x,y,z=Q
    return ((x*R/n,y*R/n,z*R/n))